<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Парсер TecDoc");
?><?$APPLICATION->IncludeComponent(
	"siteworkers:tecdoc.parser", 
	".default", 
	array(
		"DATA_EXPORT_TYPE" => "json",
		"DATA_FILE_NAME_TEMPLATE" => "",
		"FILES_EXPORT_PATH" => "/tecdoc/export",
		"IBLOCK_ID" => "13",
		"IBLOCK_TYPE" => "1c_catalog",
		"SECTIONS" => array(
			0 => "108",
			1 => "",
		),
		"TECDOC_DB_NAME" => "TecDoc",
		"TECDOC_DB_PASS" => "",
		"TECDOC_DB_PORT" => "3306",
		"TECDOC_DB_SERVER" => "localhost",
		"TECDOC_DB_USER" => "root",
		"COMPONENT_TEMPLATE" => ".default",
		"FILES_SOURCE_PATH" => "/tecdoc/source/images"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>