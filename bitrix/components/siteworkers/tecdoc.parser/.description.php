<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "ID" => GetMessage("COMP_ID"),
    "NAME" => GetMessage("COMP_NAME"),
    "PATH" => array(
        "ID" => GetMessage("COMP_GROUP_ID"),
        "NAME" => GetMessage("COMP_GROUP_NAME"),
    ),
);

?>