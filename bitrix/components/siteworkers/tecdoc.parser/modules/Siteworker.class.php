<?php
namespace Bitrix\Main\tecdoc\parser;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main,
    Bitrix\Iblock,
    CUtil;

class Siteworker
{
    private $arParams = array();

    function __construct($arParams)
    {
        $this->arParams = $arParams;
    }

    public function execute($arResult)
    {
        $statistic = $this->getSiteStatistic();
        //$statistic = array();

        $dataPath = $_SERVER["DOCUMENT_ROOT"]."/".$this->arParams["FILES_EXPORT_PATH"];
        $files = array_diff(scandir($dataPath), array('..', '.'));
        foreach ($files as $f => $file)
        {
            $explode = explode(".", $file);
            if (isset($explode[1]) && ($explode[1] === "json"))
            {
                $arResult = json_decode(file_get_contents($dataPath."/".$file),true);

                $this->updateElements($arResult, $statistic, false);
            }
        }

        foreach ($arResult as $d => $data)
        {
            $this->updateElements($data, $statistic, false);
        }
    }

    public function exportData($arResult)
    {
        $source = $_SERVER["DOCUMENT_ROOT"]."".$this->arParams["FILES_SOURCE_PATH"]."/";
        $dst    = $_SERVER["DOCUMENT_ROOT"]."".$this->arParams["FILES_EXPORT_PATH"]."/";

        $chunks = array_chunk($arResult, 100, true);
        foreach ($chunks as $c => $chunk) {
            $json = json_encode($chunk);

            switch ($this->arParams["DATA_EXPORT_TYPE"]){
                case "json":
                    file_put_contents($dst . "data" . $c . ".json", $json);
                    break;
                case "xml":
                    break;
                default:
                    file_put_contents($dst . "data" . $c . ".json", $json);
            }
        }

        foreach ($arResult as $d => $data) {
            $fullPath = preg_replace("[[\/]+]", "/", $dst);
            $data = current($data);

            if ($data["article"]) {
                $srcLink = "";
                $dstLink = "";

                if (isset($data["images"])) {
                    foreach ($data["images"] as $i => $image) {
                        $temp = explode(".", strtolower($image));
                        if ((in_array("jpg", $temp)) || (in_array("png", $temp)) || (in_array("bmp", $temp))) {
                            $folder = explode("_", strtolower($image));

                            $srcLink = $_SERVER["DOCUMENT_ROOT"]."".$this->arParams["FILES_SOURCE_PATH"]."/".$folder[0]."/".$temp[0].".jpg";
                            $dstLink = $fullPath."".$temp[0].".jpg";

                            break;
                        }
                    }
                }

                if (!file_exists($dstLink)) {
                    copy($srcLink, $dstLink);
                }
            }
        }

        return false;
    }

    private function parseString($string)
    {
        //preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $element["name"]);
        $result = preg_replace('/[\s\_\-\+\(\)\=\*\$\#\@\!\:\;\,]/', '', $string);

        return $result;
    }

    public function getSiteData($id = "")
    {
        $result = array();

        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_CML2_ARTICLE");

        if (empty($id)) {
            $arFilter = Array(/*"PROPERTY_IS_UPDATED" => 1,*/
                "SECTION_ID" => array_diff($this->arParams["SECTIONS"], array("")),
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                "ACTIVE_DATE" => "Y",
                "ACTIVE" => "Y");
        } else {
            $arFilter = Array(/*"PROPERTY_IS_UPDATED" => 1,*/
                "ID" => $id,
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                "ACTIVE_DATE" => "Y",
                "ACTIVE" => "Y");
        }

        $res = \CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
        $ch = 0;
        while ($arFields = $res->GetNext()) {
            $result[$arFields["PROPERTY_CML2_ARTICLE_VALUE"]]["id"] = $arFields["ID"];
            $result[$arFields["PROPERTY_CML2_ARTICLE_VALUE"]]["article"] = $this->parseString($arFields["PROPERTY_CML2_ARTICLE_VALUE"]);

            ++$ch;
        }

        return $result;
    }

    private function getElementSection($data)
    {
        $arrFilter = array();
        if (!is_array($data)) {
            $arFilter = Array('IBLOCK_ID'=>$this->arParams["IBLOCK_ID"], "ID" => $data, 'GLOBAL_ACTIVE'=>'Y');
        } else {
            (!empty($data)) ? $arFilter = Array('IBLOCK_ID'=>$this->arParams["IBLOCK_ID"], "ID" => $data[0]["id"], 'GLOBAL_ACTIVE'=>'Y') : $arFilter = array();
        }
        $arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID");

        $res = \CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
        while($arFields = $res->GetNext()) {
            if (isset($arFields["IBLOCK_SECTION_ID"]))
            {
                return $arFields["IBLOCK_SECTION_ID"];
            }
        }
        return false;
    }

    private function updateElements($array, $statistic, $insertProps)
    {
        //$siteProperty = preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $element["name"]);
        $is_updated = false;

        if (is_array($array) && count($array)) {
            foreach ($array as $arr) {
                foreach ($arr as $data) {
                    if (!empty($statistic)) {
                        if ($is_updated) {
                            break;
                        }

                        if (!isset($statistic[$this->section][strtolower($data["name"])])) {
                            continue;
                        } else {
                            $is_updated = true;
                        }
                    }

                    $props = array();

                    if ($insertProps) {
                        $this->insertProperties($data["id"], $data["params"]);

                        foreach ($data["params"] as $p => $param) {
                            $propCode = Cutil::translit($param["article_attributes_description"], "ru", array(
                                "change_case" => "U",
                            ));

                            $props["PRM_" . $propCode] = $param["article_attributes_displayvalue"];
                        }
                    }

                    if (!empty($data)) {
                        $path = $_SERVER["DOCUMENT_ROOT"] . "" . $this->arParams["FILES_SOURCE_PATH"];
                        $section = $this->getElementSection($data);
                        $fullPath = $path . "" . $this->arParams["FILES_EXPORT_PATH"] . "/" . $section;

                        if ($section) {
                            $fullPath = preg_replace("[[\/]+]", "/", $fullPath);

                            //$data = current($data);

                            if ($data["article"]) {
                                $srcLink = "";
                                $dstLink = "";

                                if (isset($data["images"])) {
                                    foreach ($data["images"] as $i => $image) {
                                        $temp = explode(".", strtolower($image));
                                        if ((in_array("jpg", $temp)) || (in_array("png", $temp)) || (in_array("bmp", $temp))) {
                                            $folder = explode("_", strtolower($image));

                                            $srcLink = $path . "" . $this->arParams["FILES_EXPORT_PATH"] . "/" . $folder[0] . "/" . $temp[0] . ".jpg";
                                            $dstLink = $fullPath . "/" . $temp[0] . ".jpg";

                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        $props["CML2_ARTICLE"] = $data["article"];
                        $props["IS_UPDATED"] = 2;

                        $paramTable = $this->getParamTableBlock($data["params"]);
                        $applTable = $this->getApplicTableBlock($data["applicability"]);

                        $detail = "";

                        if ($paramTable) {
                            $detail .= $paramTable . "<br><br>";
                        }

                        if ($applTable) {
                            $detail .= $applTable;
                        }

                        if (isset($data["applicability"]) && !empty($data["applicability"])) {
                            $appArray = array();

                            foreach ($data["applicability"] as $a => $applic) {
                                $appArray[$a]["VALUE"] = $applic["fulldescription"];
                                $appArray[$a]["DESCRIPTION"] = $applic["constructioninterval"];
                            }

                            //$ibElement->SetPropertyValues($data["id"], $this->iblock, $appArray, "APPLICABILITY");
                            $props["APPLICABILITY"] = $appArray;
                        }

                        if (isset($data["params"]) && !empty($data["params"])) {
                            $appArray = array();

                            foreach ($data["params"] as $p => $param) {
                                $appArray[$p]["VALUE"] = $param["article_attributes_description"];
                                $appArray[$p]["DESCRIPTION"] = $param["article_attributes_displayvalue"];
                            }

                            //$ibElement->SetPropertyValues($data["id"], $this->iblock, $appArray, "PARAMETERS");
                            $props["PARAMETERS"] = $appArray;
                        }

                        $ibElement = new \CIBlockElement();
                        $arLoadProductArray = Array(
                            "ACTIVE" => "Y",
                            "PREVIEW_TEXT_TYPE" => "html",
                            "PREVIEW_TEXT" => $data["name"],
                            "DETAIL_TEXT_TYPE" => "html",
                            "DETAIL_TEXT" => $detail,
                            "PROPERTY_VALUES" => $props,
                            "PREVIEW_PICTURE" => \CFile::MakeFileArray($dstLink),
                            "DETAIL_PICTURE" => \CFile::MakeFileArray($dstLink),
                        );

                        $ibElement->Update($data["id"], $arLoadProductArray);
                    }
                }
            }
        }

        return true;
    }

    private function insertProperties($id, $props)
    {
        $elem = new \CIBlockElement();

        foreach ($props as $p => $prop) {
            $prop = get_object_vars($prop);

            $propCode = Cutil::translit("PRM_".$prop["article_attributes_description"], "ru", array(
                "change_case" => "U",
            ));

            $isProp = false;

            $prp = $elem->GetProperty($this->iblock, $id, array(), array(), array("NAME" => $propCode));
            while ($pr = $prp->GetNext()) {
                if ($propCode === $pr["CODE"]) {
                    $isProp = true;
                    break;
                }
            }

            if (!$isProp) {
                $arFields = Array(
                    "NAME" => $prop["article_attributes_description"],
                    "ACTIVE" => "Y",
                    "SORT" => "500",
                    "CODE" => $propCode,
                    "PROPERTY_TYPE" => "S",
                    "IBLOCK_ID" => $this->iblock,
                );

                $ibp = new \CIBlockProperty;
                $propID = $ibp->Add($arFields);
            }
        }
    }

    /*
     * Формирует таблицу с параметрами товара в виде тега table
     */
    private function getParamTableBlock($paramArray) {
        $strResult = "";
        $strResult .= "<table class='param-table'>";
        $strResult .= "<tr><th>Параметр</th><th>Значение</th></tr>";

        foreach ($paramArray as $p => $param) {
            $param = get_object_vars($param);
            $strResult .= "<tr class='param-table-row'>";
            $strResult .= "<td class='param-table-col'>".$param["article_attributes_description"]."</td><td class='param-table-col'>".$param["article_attributes_displayvalue"]."</td>";
            $strResult .= "</tr>";
        }

        $strResult .= "</table>";

        return $strResult;
    }

    private function getApplicTableBlock($paramArray) {
        $strResult = "";
        $strResult .= "<table class='param-table'>";
        $strResult .= "<tr><th>Модель</th><th>Год выпуска</th></tr>";

        foreach ($paramArray as $p => $param) {
            $param = get_object_vars($param);
            $strResult .= "<tr class='param-table-row'>";
            $strResult .= "<td class='param-table-col'>".$param["fulldescription"]."</td><td class='param-table-col'>".$param["constructioninterval"]."</td>";
            $strResult .= "</tr>";
        }

        $strResult .= "</table>";

        return $strResult;
    }

    private function getSiteStatistic()
    {
        $statArray = array($this->section => array());

        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_CML2_ARTICLE");
        $arFilter = Array(
            "SECTION_ID" => $this->arParams["SECTIONS"],
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y");

        $res = \CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
        while ($arFields = $res->GetNext())
        {
            $nameArray = explode(" ", $arFields["NAME"]);
            foreach ($nameArray as $n => $name)
            {
                $name = strtolower($name);
                if (strlen($name) > 5) {
                    if (!isset($statArray[$this->section][$name])) {
                        $statArray[$this->section][$name] = 1;
                    } else {
                        $statArray[$this->section][$name] = ++$statArray[$this->section][$name];
                    }
                }
            }
        }

        arsort($statArray[$this->section]);

        return $statArray;
    }

}