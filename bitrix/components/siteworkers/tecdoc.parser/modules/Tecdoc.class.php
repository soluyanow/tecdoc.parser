<?php
namespace Bitrix\Main\tecdoc\parser;

class Tecdoc extends Database
{
    function __construct($dbParams)
    {
        parent::__construct($dbParams);
    }

    private function buildSelectFields($selectArray, $dbName, $eqType)
    {
        if (is_array($selectArray))
        {
            if (count($selectArray) === 0) {
                return "";
            }

            $select = "";

            $select = "WHERE ";
            if (count($selectArray) === 1)
            {
                $select .= $dbName.".".(key($selectArray))." ".$eqType." '".current($selectArray)."'";
            }
            else if (count($selectArray) > 1)
            {
                $cnt = 0;
                foreach ($selectArray as $s => $val) {
                    if ($cnt > 0) {
                        $select .= " AND ";
                    }
                    $select .= "(";
                    $select .= $dbName.".".$s." ".$eqType." '".$val."'";
                    $select .= ")";

                    ++$cnt;
                }
            }

            return $select;
        }

        return "";
    }

    private function buildFieldLine($fieldArray)
    {
        $fields = "";
        is_array($fieldArray) ? $fields = implode(", ", $fieldArray) : $fields = $fieldArray;

        return $fields;
    }

    public function buildSimpleIndex($dbName, $table, $field)
    {
        //CREATE index articles_DataSupplierArticleNumber on TecDoc.articles(DataSupplierArticleNumber);

        $query = "CREATE INDEX ".$table."_".$field." ON ".$dbName.".".$table."(".$field.")";

        $this->index($query);
    }

    public function buildComplexIndex($dbName, $table, $fields)
    {


    }

    public function getFullArticle($queryResult, $fieldName)
    {
        $result = false;

        if (is_array($queryResult) && isset($queryResult[0][$fieldName]))
        {
            $result = $queryResult[0][$fieldName];
        }

        return $result;
    }

    public function selectMainInfo($selectArray = array(), $fieldArray = array("*"), $dbName = "TecDoc", $eqType = "=", $limit = 1000)
    {
        $fields = self::buildFieldLine($fieldArray);
        $select = self::buildSelectFields($selectArray, $dbName, $eqType);

        $query = "SELECT ".$fields."
                  FROM ".$dbName.".articles
                  #INNER JOIN ".$dbName.".article_images ON ".$dbName.".articles.DataSupplierArticleNumber = ".$dbName.".article_images.DataSupplierArticleNumber
                  #INNER JOIN ".$dbName.".article_attributes ON ".$dbName.".articles.DataSupplierArticleNumber = ".$dbName.".article_attributes.datasupplierarticlenumber
                  #INNER JOIN ".$dbName.".article_li ON ".$dbName.".articles.DataSupplierArticleNumber = ".$dbName.".article_li.DataSupplierArticleNumber
                  #INNER JOIN ".$dbName.".article_links ON (".$dbName.".article_li.linkageId = ".$dbName.".article_links.linkageid) and (".$dbName.".article_li.DataSupplierArticleNumber = ".$dbName.".article_links.datasupplierarticlenumber)
                  ".$select."
                  LIMIT ".$limit."";

        $result = $this->select($query);

        return $result;
    }

    /*
     * @param array $select
     */
    public function selectAdditionalInfo($tableType, $selectArray = array(), $eqType = "=", $dbName = "TecDoc", $fieldArray = array("*"), $limit = 1000)
    {
        $result = array();

        $tables = "";
        $table = "";
        $additionalQueries = "";

        switch (strtolower($tableType))
        {
            case "passengercar": //PassengerCar
                $tables     = "passanger_cars";
                $table      = "passanger_car";

                break;
            case "commercialvehicle": //CommercialVehicle
                $tables     = "commercial_vehicles";
                $table      = "commercial_vehicle";

                break;
            case "axle": //Axle
                $tables     = "axles";
                $table      = "axle";

                break;
            case "engine": //Engine
                $tables     = "engines";
                $table      = "engine";

                break;
            case "motorbike": //Motorbike
                $tables     = "motorbikes";
                $table      = "motorbike";

                break;
            default:
                return array();
        }

        $fields = self::buildFieldLine($fieldArray);
        $select = self::buildSelectFields($selectArray, $dbName, $eqType);

        $query = "SELECT ".$fields." 
                    FROM ".$dbName.".".$tables."
                    INNER JOIN ".$dbName.".".$table."_attributes ON ".$dbName.".".$tables.".id = ".$dbName.".".$table."_attributes.passangercarid
                    INNER JOIN ".$dbName.".".$table."_engines ON ".$dbName.".".$tables.".id = ".$dbName.".".$table."_engines.id
                    INNER JOIN ".$dbName.".".$table."_pds ON ".$dbName.".".$tables.".id = ".$dbName.".".$table."_pds.passangercarid
                    INNER JOIN ".$dbName.".".$table."_prd ON ".$dbName.".".$tables.".id = ".$dbName.".".$table."_prd.id                    
                    INNER JOIN ".$dbName.".".$table."_trees ON ".$dbName.".".$tables.".id = ".$dbName.".".$table."_trees.passangercarid
                    ".$additionalQueries."
                    ".$select."
                    limit ".$limit;

        return $this->select($query);
    }

    public function doSimpleQuery($dbName, $table, $selectArray, $fields, $eqType = "=")
    {
        $query = "SELECT ".$fields." FROM ".$dbName.".".$table." WHERE ".$dbName.".".$table.".".key($selectArray)." = '".current($selectArray)."'";

        return $this->select($query);
    }


    /*--------------------------------*/

    public function selectApplicability($selectArray = array(), $fieldArray = array("*"), $dbName = "TecDoc", $eqType = "=", $limit = 1000)
    {
        /*'0284-001'*/

        $fields = $this->buildFieldLine($fieldArray);
        $select = $this->buildSelectFields($selectArray, $dbName, $eqType);

        $query = "SELECT ".$fields."
                    FROM ".$dbName.".articles                    
                    INNER JOIN ".$dbName.".article_links ON ".$dbName.".article_links.supplierid = ".$dbName.".articles.supplierId AND TecDoc.article_links.datasupplierarticlenumber = TecDoc.articles.DataSupplierArticleNumber
                    INNER JOIN ".$dbName.".passanger_cars ON ".$dbName.".passanger_cars.id = ".$dbName.".article_links.linkageid
                    INNER JOIN ".$dbName.".models AS m ON ".$dbName.".passanger_cars.modelid = m.id
                    INNER JOIN ".$dbName.".suppliers ON ".$dbName.".suppliers.id = ".$dbName.".articles.supplierId
                    ".$select."
                    LIMIT ".$limit;

        $result = $this->select($query);

        return $result;
    }
}