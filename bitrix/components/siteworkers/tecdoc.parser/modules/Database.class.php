<?php
namespace Bitrix\Main\tecdoc\parser;
use mysqli;

class Database
{
    private $params;
    private $instance;
    public function __construct($params)
    {
        $this->params = $params;
    }

    private function instance()
    {
        if (!isset($instance)) {
            $this->connect($this->params);
        }
        return $this->instance;
    }

    public function connect(array $params)
    {
        $mysqli = new mysqli($params['host'], $params['user'], $params['pass'], $params['name'], $params['port']);

        if (!$mysqli->connect_error)
        {
            $mysqli->set_charset("utf8");
            $this->instance = $mysqli;
        }
        return $mysqli;
    }

    public function query($query)
    {
        return $this->instance()->query($query);
    }

    public function select($query)
    {
        $mysqli_result = $this->instance()->query($query);
        if ($mysqli_result)
        {
            $result = array();
            while ($row = $mysqli_result->fetch_object()) {
                is_array($row) ? ($result[] = $row) : ($result[] = get_object_vars($row));
            }
            return $result;
        }
        return array();
    }

    public function index($query)
    {
        if ($this->instance()->query($query))
        {
            return true;
        }

        return false;
    }
}