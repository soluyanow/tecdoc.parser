<?php
namespace Bitrix\Main\tecdoc\parser;

class ParseResult
{
    private static function getElementStructure()
    {
        return [
            "id"            => "",
            "name"          => "",
            "article"       => "",
            "images"        => array(),
            "params"        => array(),
            "applicability" => array(),
        ];
    }

    public function parseInfo($array, $type, $fields)
    {
        $arResult = array();

        switch ($type)
        {
            case "main":
                foreach ($array as $r => $arr)
                {
                    foreach ($arr as $i => $value)
                    {
                        if (!isset($arResult[$r][$i])) {
                            $arResult[$i] = self::getElementStructure();
                        }

                        $arResult[$i]["id"] = $fields["id"];
                        $arResult[$i]["name"] = $value["articles_NormalizedDescription"];
                        $arResult[$i]["article"] = $value["articles_DataSupplierArticleNumber"];
                        //$arResult[$i]["name"] = $fields["articles_NormalizedDescription"];
                        //$arResult[$i]["article"] = $fields["articles_DataSupplierArticleNumber"];
                    }
                }

                return $arResult;

                break;
            case "images":
                $arResult = array();
                foreach ($array as $r => $arr)
                {
                    $arResult[$r] = $arr;
                    foreach ($arr as $a => $ar)
                    {
                        foreach ($fields as $f => $field) {
                            $arResult[$r]["images"][$f] = $field["article_images_PictureName"];
                        }
                    }
                }

                return $arResult;

                break;
            case "params":
                $arResult = array();
                foreach ($array as $r => $arr)
                {
                    $arResult[$r] = $arr;
                    foreach ($arr as $a => $ar)
                    {
                        foreach ($fields as $f => $field)
                        {
                            $arResult[$r]["params"][$f] = $field;
                        }
                    }
                }

                return $arResult;

                break;
            case "applic":
                $arResult = array();
                foreach ($array as $r => $arr)
                {
                    $arResult[$r] = $arr;
                    foreach ($arr as $a => $ar)
                    {
                        foreach ($fields as $f => $field)
                        {
                            $arResult[$r]["applicability"][$f]["description"] = $field["description"];
                            $arResult[$r]["applicability"][$f]["constructioninterval"] = $field["constructioninterval"];
                            $arResult[$r]["applicability"][$f]["fulldescription"] = $field["fulldescription"];
                        }
                    }
                }

                return $arResult;

                break;
            default:
                return array();
        }

        return $arResult;
    }

    public function parseResultArray($resultArray, $elementArray)
    {
        $tempArray = array();

        $article     = "articles_FoundString";
        $fullArticle = "articles_DataSupplierArticleNumber";
        $name        = "articles_NormalizedDescription";

        foreach ($resultArray as $r => $item) {
            $temp = array();
            (is_object($item)) ? $temp = get_object_vars($item) : $temp = $item;

            if (!empty($temp)) {
                $art = preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $temp[$article]);

                if (($elementArray["article"] === $temp[$article]) || ($elementArray["article"] === preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $temp[$fullArticle]))) {
                    $tempArray[$art][] = $temp;
                    //$tempArray[$art][] =["id"] = $elementArray["id"];
                    /*$tempArray[$art][$article] = str_replace(" ", "", $temp[$article]);
                    $tempArray[$art][$fullArticle] = $temp[$fullArticle];
                    $tempArray[$art][$name] = $temp[$name];*/
                }
            }

            foreach ($elementArray as $e => $value)
            {
                /*$tmp = preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $temp);
                $val = preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $value);*/

                /*if (isset($temp[$e]) && ($temp[$e] === $value))
                {
                    $tempArray[$elementArray["id"]][] = $value;
                    break;
                }*/
            }
        }

        return self::parseInfo($tempArray, "main", $elementArray);
    }
}

/*

$tempArray = array();

        foreach ($resultArray as $r => $item) {
            $temp = array();
            (is_object($item)) ? $temp = get_object_vars($item) : $temp = $item;

            foreach ($elementArray as $e => $value)
            {
                if (isset($temp[$e]) && ($temp[$e] === $value))
                {
                    $tempArray[$elementArray["id"]][] = $temp;
                }
            }
        }

        return self::parseInfo($tempArray, "main", $elementArray);

 */