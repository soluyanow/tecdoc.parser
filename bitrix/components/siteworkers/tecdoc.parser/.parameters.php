<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\Web\Json,
    Bitrix\Iblock,
    Bitrix\Catalog,
    Bitrix\Currency;

global $USER_FIELD_MANAGER;

if (!Loader::includeModule('iblock'))
    return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$ibFilter = !empty($arCurrentValues['IBLOCK_TYPE'])
    ? array('TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE' => 'Y')
    : array('ACTIVE' => 'Y');

$rsIBlock = CIBlock::GetList(array('SORT' => 'ASC'), $ibFilter);
while ($arr = $rsIBlock->Fetch())
{
    $id = (int)$arr['ID'];
    if (isset($offersIblock[$id]))
        continue;
    $arIBlock[$id] = '['.$id.'] '.$arr['NAME'];
}

$sections = array();

$arFilter = Array('IBLOCK_ID' => IntVal($arCurrentValues["IBLOCK_ID"]), 'GLOBAL_ACTIVE'=>'Y');
$res = CIBlockSection::GetList(array(), $arFilter, true);
while($arFields = $res->GetNext())
{
    $sections[$arFields["ID"]] = '['.$arFields["ID"].'] '.$arFields['NAME'];
}

$arComponentParameters = array(
    'PARAMETERS' => array(
        'IBLOCK_TYPE' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('IBLOCK_TYPE'),
            'TYPE' => 'LIST',
            'VALUES' => $arIBlockType,
            'REFRESH' => 'Y',
        ),
        'IBLOCK_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('IBLOCK_ID'),
            'TYPE' => 'LIST',
            'VALUES' => $arIBlock,
            'REFRESH' => 'Y',
        ),
        'SECTIONS' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('SECTIONS_ID'),
            'TYPE' => 'LIST',
            'MULTIPLE' => 'Y',
            'ADDITIONAL_VALUES' => 'Y',
            'VALUES' => $sections,
        ),
        'TECDOC_DB_SERVER' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('TECDOC_DB_SERVER'),
            'TYPE' => 'STRING',
            'VALUES' => "",
            'DEFAULT' => "localhost",
        ),
        'TECDOC_DB_NAME' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('TECDOC_DB_NAME'),
            'TYPE' => 'STRING',
            'VALUES' => "",
            'DEFAULT' => "TecDoc",
        ),
        'TECDOC_DB_PASS' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('TECDOC_DB_PASS'),
            'TYPE' => 'STRING',
            'VALUES' => "",
            'DEFAULT' => "",
        ),
        'TECDOC_DB_PORT' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('TECDOC_DB_PORT'),
            'TYPE' => 'STRING',
            'VALUES' => "",
            'DEFAULT' => "3306",
        ),
        'DATA_EXPORT_TYPE' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('DATA_EXPORT_TYPE'),
            'TYPE' => 'STRING',
            'VALUES' => "",
            'DEFAULT' => "json",
        ),
        'DATA_FILE_NAME_TEMPLATE' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('DATA_FILE_NAME_TEMPLATE'),
            'TYPE' => 'STRING',
            'VALUES' => "",
            'DEFAULT' => "data#.".isset($arCurrentValues["DATA_EXPORT_TYPE"]) ? $arCurrentValues["DATA_EXPORT_TYPE"] : "json",
        ),

        'FILES_SOURCE_PATH' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('FILES_SOURCE_PATH'),
            'TYPE' => 'STRING',
            'VALUES' => "",
            'DEFAULT' => "",
        ),
        'FILES_EXPORT_PATH' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('FILES_EXPORT_PATH'),
            'TYPE' => 'STRING',
            'VALUES' => "",
            'DEFAULT' => "",
        ),

    ),
);