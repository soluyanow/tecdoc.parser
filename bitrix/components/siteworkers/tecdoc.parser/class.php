<?php
namespace Bitrix\Main\tecdoc\parser;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use \Bitrix\Main;
use \Bitrix\Main\Error;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;

global $APPLICATION;

$modulesRoot = $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/siteworkers/tecdoc.parser/modules";
require($modulesRoot."/Database.class.php");
require($modulesRoot."/Tecdoc.class.php");
require($modulesRoot."/ParseResult.class.php");
require($modulesRoot."/SiteWorker.class.php");

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
    ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
    return;
}

class tecdocparser Extends \CBitrixComponent
{
    private function parseString($string)
    {
        //preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $element["name"]);
        $result = preg_replace('/[\s\_\-\+\(\)\=\*\$\#\@\!\:\;\,]/', '', $string);

        return $result;
    }

    private function getDbFieldArray()
    {
        if (isset($this->arParams["TECDOC_DB_NAME"])) {
            $dbName = $this->arParams["TECDOC_DB_NAME"];

            $fieldArray = array();

            $fieldArray[] = $dbName . ".articles.supplierId AS articles_supplierId";
            $fieldArray[] = $dbName . ".articles.DataSupplierArticleNumber AS articles_DataSupplierArticleNumber";
            $fieldArray[] = $dbName . ".articles.ArticleStateDisplayValue AS articles_ArticleStateDisplayValue";
            $fieldArray[] = $dbName . ".articles.Description AS articles_Description";
            $fieldArray[] = $dbName . ".articles.FlagAccessory AS articles_FlagAccessory";
            $fieldArray[] = $dbName . ".articles.FoundString AS articles_FoundString";
            $fieldArray[] = $dbName . ".articles.HasAxle AS articles_HasAxle";
            $fieldArray[] = $dbName . ".articles.HasCommercialVehicle AS articles_HasCommercialVehicle";
            $fieldArray[] = $dbName . ".articles.HasCVManuID AS articles_HasCVManuID";
            $fieldArray[] = $dbName . ".articles.HasEngine AS articles_HasEngine";
            $fieldArray[] = $dbName . ".articles.HasLinkitems AS articles_HasLinkitems";
            $fieldArray[] = $dbName . ".articles.HasMotorbike AS articles_HasMotorbike";
            $fieldArray[] = $dbName . ".articles.HasPassengerCar AS articles_HasPassengerCar";
            $fieldArray[] = $dbName . ".articles.NormalizedDescription AS articles_NormalizedDescription";
            /*$fieldArray[] = $dbName.".article_images.PictureName AS article_images_NormalizedDescription";
            $fieldArray[] = $dbName.".article_attributes.id AS article_attributes_id";
            $fieldArray[] = $dbName.".article_attributes.description AS article_attributes_description";
            $fieldArray[] = $dbName.".article_attributes.displaytitle AS article_attributes_displaytitle";
            $fieldArray[] = $dbName.".article_attributes.displayvalue AS article_attributes_displayvalue";
            $fieldArray[] = $dbName.".article_li.supplierId AS article_li_supplierId";
            $fieldArray[] = $dbName.".article_li.linkageTypeId AS article_li_linkageTypeId";
            $fieldArray[] = $dbName.".article_li.linkageId AS article_li_linkageId";
            $fieldArray[] = $dbName.".article_links.productid AS article_links_productid";
            $fieldArray[] = $dbName.".article_links.linkagetypeid AS article_links_linkagetypeid";
            $fieldArray[] = $dbName.".article_links.linkageid AS article_links_linkageid";*/

            return $fieldArray;
        }

        return array();
    }

    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $this->includeComponentTemplate();
        }

        $swObject = new SiteWorker($this->arParams);
        $siteData = $swObject->getSiteData(); //Получить данные о товарах
        $dbFields = $this->getDbFieldArray(); //Получить поля для запроса

        $dir_path = $this->arResult["FILES_EXPORT_PATH"];
        $dbName = $this->arParams["TECDOC_DB_NAME"];
        $ch = 0;

        $dbCon = array(
            "host" => (isset($this->arParams["TECDOC_DB_SERVER"])) ? $this->arParams["TECDOC_DB_SERVER"] : "",
            "port" => (isset($this->arParams["TECDOC_DB_PORT"])) ? $this->arParams["TECDOC_DB_PORT"] : "",
            "base" => (isset($this->arParams["TECDOC_DB_NAME"])) ? $this->arParams["TECDOC_DB_NAME"] : "",
            "user" => (isset($this->arParams["TECDOC_DB_USER"])) ? $this->arParams["TECDOC_DB_USER"] : "",
            "pass" => (isset($this->arParams["TECDOC_DB_PASS"])) ? $this->arParams["TECDOC_DB_PASS"] : "",
        );

        $tdObject = new Tecdoc($dbCon);
        $prObject = new ParseResult();

        foreach ($siteData as $e => $element) {
            try {
                $eqType = "=";
                $f0String = "articles.FoundString";
                $f1String = "articles_DataSupplierArticleNumber";

                $mainInfo = $tdObject->selectMainInfo(array($f0String => $e), $dbFields, "TecDoc", $eqType, 100000);
                if ($mainInfo) {
                    $fullArticle = $tdObject->getFullArticle($mainInfo, $f1String);
                    $applic = $tdObject->selectApplicability(array("articles.DataSupplierArticleNumber" => $fullArticle), array("*"), "TecDoc", $eqType);

                    $tableName = "article_images";
                    $fields = $dbName . "." . $tableName . ".DataSupplierArticleNumber AS " . $tableName . "_DataSupplierArticleNumber, " .
                        $dbName . "." . $tableName . ".PictureName AS " . $tableName . "_PictureName";
                    $images = $tdObject->doSimpleQuery($dbName, $tableName, array("DataSupplierArticleNumber" => $fullArticle), $fields, $eqType);

                    $tableName = "article_attributes";
                    $fields = $dbName . "." . $tableName . ".datasupplierarticlenumber AS " . $tableName . "_datasupplierarticlenumber, " .
                        $dbName . "." . $tableName . ".id AS " . $tableName . "_id, " .
                        $dbName . "." . $tableName . ".description AS " . $tableName . "_description, " .
                        $dbName . "." . $tableName . ".displaytitle AS " . $tableName . "_displaytitle, " .
                        $dbName . "." . $tableName . ".displayvalue AS " . $tableName . "_displayvalue";
                    $attribs = $tdObject->doSimpleQuery($dbName, $tableName, array("DataSupplierArticleNumber" => $fullArticle), $fields, $eqType);

                    $fullArticle = preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $fullArticle);

                    $this->arResult[$fullArticle] = $prObject->parseResultArray($mainInfo, $element);
                    $this->arResult[$fullArticle] = $prObject->parseInfo($this->arResult[$fullArticle], "images", $images);
                    $this->arResult[$fullArticle] = $prObject->parseInfo($this->arResult[$fullArticle], "params", $attribs);
                    $this->arResult[$fullArticle] = $prObject->parseInfo($this->arResult[$fullArticle], "applic", $applic);
                    ++$ch;
                }
            } catch (\Exception $e) {

            }
        }

        if ($this->arResult) {
            $swObject->exportData($this->arResult);
            $swObject->execute($this->arResult);
            return true;
        } else {
            return false;
        }

        return false;
    }






}



















